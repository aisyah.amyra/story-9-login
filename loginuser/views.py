from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.conf import settings

# Create your views here.

def login_page(request):
    if request.user.is_authenticated:
        return redirect('welcome')
    else:
        if request.method == 'GET':
            return render(request,'login.html')
        elif request.method == 'POST':
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                request.session['nama']= request.user.first_name + " " + request.user.last_name
                return redirect('welcome')
            else:
                return redirect('home')

def welcome(request):
    if not request.user.is_authenticated:
        return redirect('home')
    else:
        return render(request,'welcome.html')

def logout_view(request):
    logout(request)
    return redirect('home')

